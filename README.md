<!--
 * @Author: Junhui Li junhui.lee@icloud.com
 * @Date: 2023-08-26 13:15:49
 * @LastEditors: Junhui Li junhui.lee@icloud.com
 * @LastEditTime: 2023-08-26 13:27:44
 * @Description: 
-->
# 要求
1. 头文件在 `include`，源文件在 `src`，单元测试在 `test`
2. 命令实现的函数声明放到 `cmd.h` 里，自己实现功能新建一个文件，比如 `cd_pwd.c` 实现两个命令
3. 不允许修改其他人的源码
4. 两方协定的数据结构和结构只允许一人修改
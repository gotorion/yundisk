#ifndef __CMD_H__
#define __CMD_H__
#include "func.h"
// #include "client.h"
#define MSG_LENGTH 512
typedef enum {
  SERVER_OK,
  SERVER_ERR,
  LOCAL_EXIST,
  LOCAL_NOT_EXIST
} status_t;

int do_cmd(int sockfd, char *args[], char *pwd);

int do_ls(int sockfd); // 和服务端ls统一收发格式并打印信息
int do_pwd(int sockfd); 
int do_mkdir(int sockfd);
int do_rm(int sockfd);
int do_rmdir(int sockfd);
int do_cd(int sockfd, char *pwd);
int do_puts(int sockfd, const char *filename); // arg 是 filename 
int do_gets(int sockfd, const char *filename);

#endif
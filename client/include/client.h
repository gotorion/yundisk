#ifndef __CLIENT_H__
#define __CLIENT_H__
#include "func.h"
#include "cmd.h"
typedef struct train_s {
  int length;
  char data[4096];
} train_t;
int user_verify(int sockfd);
int get_arg(char *src, char *args[]);
int init_tcp(int *psockfd, char *port, char *addr);
int send_cmd(int sockfd, char *args[]);
int get_remain(int sockfd, int fd);
#endif
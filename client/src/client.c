#include "../include/client.h"
int main(int argc, char *argv[]) {
  // ./client addr port
  ARGS_CHECK(argc, 3);
  int sockfd;
  init_tcp(&sockfd, argv[2], argv[1]);

  while (true) {
    int ret = user_verify(sockfd);
    if (ret == 0) {
      break;
    } else {
      printf("Invalid username or password, enter again!\n");
      continue;
    }
  }

  char pwd[512] = {0};  // 保存用户当前路径
  strcpy(pwd, "~/");
  while (true) {
    printf("\033[1;32m%s\033[0;31m$\033[1;37m", pwd);
    fflush(stdout);
    char *line = NULL;
    size_t size = 0;
    ssize_t end = getline(&line, &size, stdin);
    line[end - 1] = '\0';
    char *tok[2] = {NULL, NULL};
    int ret = get_arg(line, tok);
    if (ret == -1) {
      printf("Invalid cmd or args, enter again!\n");
      free(line);
      continue;
    }
    send_cmd(sockfd, tok);
    do_cmd(sockfd, tok, pwd);
    free(line);
  }
  return 0;
}
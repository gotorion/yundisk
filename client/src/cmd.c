#include "../include/cmd.h"

#include "../include/client.h"
#include "../include/func.h"
int do_cmd(int sockfd, char **args, char *pwd) {
  if (strcmp(args[0], "cd") == 0) {
    do_cd(sockfd, pwd);
  } else if (strcmp(args[0], "pwd") == 0) {
    do_pwd(sockfd);
  } else if (strcmp(args[0], "ls") == 0) {
    do_ls(sockfd);
  } else if (strcmp(args[0], "rm") == 0) {
    do_rm(sockfd);
  } else if (strcmp(args[0], "mkdir") == 0) {
    do_mkdir(sockfd);
  } else if (strcmp(args[0], "rmdir") == 0) {
    do_rm(sockfd);
  } else if (strcmp(args[0], "gets") == 0) {
    do_gets(sockfd, args[1]);
  } else if (strcmp(args[0], "puts") == 0) {
    do_puts(sockfd, args[1]);
  }
  return 0;
}

int do_cd(int sockfd, char *pwd) {
  status_t status;
  recv(sockfd, &status, sizeof(status_t), 0);
  if (status == SERVER_OK) {
    bzero(pwd, MSG_LENGTH);
    int length = 0;
    int n = recv(sockfd, &length, sizeof(length), MSG_WAITALL);
    n = recv(sockfd, pwd, length, MSG_WAITALL);
    pwd[n] = '\0';
  } else if (status == SERVER_ERR) {
    int length = 0;
    char err_msg[512] = {0};
    recv(sockfd, &length, sizeof(length), MSG_WAITALL);
    recv(sockfd, err_msg, length, MSG_WAITALL);
    puts(err_msg);
  }
  return (status == SERVER_OK) ? 0 : -1;
}

int do_ls(int sockfd) {
  /* 服务端通过获取通过真实路径打开目录流，如果打开成功返回SERVER_OK，然后发送目录流信息
    如果打开失败给客户端返回失败原因
  客户端先接受服务端的返回状态，如果是SERVER_OK，接受目录流信息，如果是失败将失败原因打印出来*/
  train_t train;
  while (true) {
    bzero(&train, sizeof(train));
    recv(sockfd, &train.length, sizeof(train.length), MSG_WAITALL);
    if (!train.length) {
      break;
    }
    recv(sockfd, train.data, train.length, MSG_WAITALL);

    printf("%s\n", train.data);
  }
  return 0;
}
int do_pwd(int sockfd) {
  status_t status;
  recv(sockfd, &status, sizeof(status_t), MSG_WAITALL);
  if (status == SERVER_OK) {
    char pwd[MSG_LENGTH] = {0};
    int length;
    recv(sockfd, &length, sizeof(length), MSG_WAITALL);
    recv(sockfd, pwd, length, MSG_WAITALL);
    printf("current working directory: %s\n", pwd);
  } else {
    return -1;
  }
  return 0;
}
int do_mkdir(int sockfd) {
  /* 服务端先将参数(也就是将要建立的文件夹名)和真实路径拼接得到pathname，然后调用mkdir(pathname,
  mode) 同样先返回状态信息，客户端如果收到SERVER_OK就可以直接打印成功信息*/
  status_t status;
  recv(sockfd, &status, sizeof(status_t), MSG_WAITALL);
  if (status != SERVER_OK) {
    printf("mkdir error\n");
  } else {
    printf("mkdir success\n");
  }
  return 0;
}
int do_rm(int sockfd) {
  status_t status;
  recv(sockfd, &status, sizeof(status_t), MSG_WAITALL);
  if (status != SERVER_OK) {
    printf("rm failed\n");
  } else {
    printf("rm success\n");
  }
  return 0;
}
int do_rmdir(int sockfd) { /* 同上 */
  status_t status;
  recv(sockfd, &status, sizeof(status_t), MSG_WAITALL);
  if (status != SERVER_OK) {
    printf("rmdir failed\n");
  } else {
    printf("rmdir success\n");
  }
  return 0;
}
int do_cd(int sockfd, char *pwd);  // DONE
int do_puts(int sockfd, const char *filename) {
  /* 客户端先判断本地文件是否存在，如果不存在直接报错，不用继续和服务端通信 */
  if (access(filename, F_OK) != 0) {
    printf("local file does't exist\n");
    return -1;
  }
  status_t status;
  recv(sockfd, &status, sizeof(status_t), MSG_WAITALL);
  if (status != SERVER_OK) {
    printf("puts to server error\n");
    return -1;
  }
  int fd = open(filename, O_RDWR);
  struct stat statbuf;
  fstat(fd, &statbuf);
  send(sockfd, &statbuf.st_size, sizeof(off_t), 0);
  char *p = (char *)mmap(NULL, statbuf.st_size, PROT_READ | PROT_WRITE,
                         MAP_SHARED, fd, 0);
  send(sockfd, p, statbuf.st_size, 0);
  munmap(p, statbuf.st_size);
  return 0;
}
int do_gets(int sockfd, const char *filename) { /* 原理同上*/
  status_t status = LOCAL_NOT_EXIST;
  if (access(filename, F_OK) == 0) {
    status = LOCAL_EXIST;
  }
  // 先发送本地文件是否存在
  send(sockfd, &status, sizeof(status_t), 0);
  status_t server_stat;
  recv(sockfd, &server_stat, sizeof(status_t), MSG_WAITALL);
  if (server_stat != SERVER_OK) {
    printf("server error\n");
    return -1;
  } else if (status == LOCAL_EXIST) {  // 本地和服务端都存在，做断点续传
    int fd = open(filename, O_RDWR);
    get_remain(sockfd, fd);
    return 0;
  } else {  // 本地文件不存在，新建文件接收
    int fd = open(filename, O_RDWR | O_CREAT, 0666);
    off_t file_size;
    recv(sockfd, &file_size, sizeof(off_t), MSG_WAITALL);
    ftruncate(fd, file_size);
    char *p = (char *)mmap(NULL, file_size, PROT_READ | PROT_WRITE, MAP_SHARED,
                           fd, 0);
    recv(sockfd, p, file_size, MSG_WAITALL);
    munmap(p, file_size);
  }
  return 0;
}

/* 断点续传 */
int get_remain(int sockfd, int fd) {
  struct stat statbuf;
  fstat(fd, &statbuf);
  // 发送本地文件大小给服务器
  send(sockfd, &statbuf.st_size, sizeof(off_t), 0);
  // 接受服务器文件大小
  off_t server_filesize;
  recv(sockfd, &server_filesize, sizeof(off_t), MSG_WAITALL);
  // 检查文件大小是否相同
  
  // 客户端偏移到文件末尾
  ftruncate(fd, server_filesize);
  char *p = mmap(NULL, server_filesize - statbuf.st_size,
                 PROT_READ | PROT_WRITE, MAP_SHARED, fd, statbuf.st_size);
  recv(sockfd, p, server_filesize - statbuf.st_size, MSG_WAITALL);
  munmap(p, server_filesize - statbuf.st_size);

  // status_t status;
  // recv(sockfd, &status, sizeof(status_t), MSG_WAITALL);
  // return (status == SERVER_OK) ? 0 : -1;
  return 0;
}
#include "../include/client.h"
int init_tcp(int *psockfd, char *port, char *addr) {
  *psockfd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
  ERROR_CHECK(*psockfd, -1, "socket");
  struct sockaddr_in server_addr;
  server_addr.sin_family = AF_INET;
  server_addr.sin_port = htons(atoi(port));
  server_addr.sin_addr.s_addr = inet_addr(addr);
  int ret =
      connect(*psockfd, (struct sockaddr *)&server_addr, sizeof(server_addr));
  ERROR_CHECK(ret, -1, "connect");
  return 0;
}

int user_verify(int sockfd) {
  char user_name[512] = {0};
  char password[512] = {0};
  printf("Enter username: ");
  fscanf(stdin, "%s", user_name);
  printf("Enter password: ");
  fscanf(stdin, "%s", password);
  getchar();  // 把换行读走，不然影响后续输入命令
  // 给服务端发送用户名和密码
  int length = strlen(user_name);
  send(sockfd, &length, sizeof(length), 0);
  send(sockfd, user_name, length, 0);
  length = strlen(password);
  send(sockfd, &length, sizeof(length), 0);
  send(sockfd, password, length, 0);
  // 等待服务端返回
  status_t status;
  recv(sockfd, &status, sizeof(status_t), MSG_WAITALL);
  if (status == SERVER_ERR) {
    return -1;
  }
  return 0;
}

int get_arg(char *src, char **args) {
  args[0] = strtok(src, " ");
  if (args[0] == NULL) return -1;
  if (strcmp(args[0], "ls") != 0 && strcmp(args[0], "pwd") != 0 &&
      strcmp(args[0], "mkdir") != 0 && strcmp(args[0], "rmdir") != 0 &&
      strcmp(args[0], "rm") != 0 && strcmp(args[0], "cd") != 0 &&
      strcmp(args[0], "puts") != 0 && strcmp(args[0], "gets") != 0) {
    return -1;
  }
  args[1] = strtok(NULL, " ");
  if (strcmp(args[0], "ls") == 0 || strcmp(args[0], "pwd") == 0) {  // 一参数
    if (args[1] != NULL) {
      return -1;
    }
  } else {  // 两参数
    if (args[1] == NULL) {
      return -1;
    } else {
      char *next_tok = strtok(NULL, " ");
      if (next_tok != NULL) {
        return -1;
      }
    }
  }
  return 0;
}

int send_cmd(int sockfd, char *args[]) {
  train_t train;
  bzero(&train, sizeof(train_t));
  train.length = strlen(args[0]);
  send(sockfd, &train.length, sizeof(int), 0);
  memcpy(train.data, args[0], train.length);
  send(sockfd, train.data, train.length, 0);
  if (strcmp(train.data, "ls") == 0 || strcmp(train.data, "pwd") == 0) {
    return 0;
  }
  bzero(&train, sizeof(train_t));
  train.length = strlen(args[1]);
  send(sockfd, &train.length, sizeof(train.length), 0);
  memcpy(train.data, args[1], train.length);
  send(sockfd, train.data, train.length, 0);

  return 0;
}
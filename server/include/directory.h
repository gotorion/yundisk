#ifndef __DIRECTORY_H__
#define __DIRECTORY_H__
#include "func.h"
#define DEPTH 100
#define MSG_LENGTH 512
typedef struct dir_stack_s {
  char *dirs[DEPTH]; // 最多支持100级目录
  int top;
} dir_stack_t;
// TODO: 初始化
int init_dir_stack(dir_stack_t *dir_stack);
// TODO: 入栈出栈
int push(dir_stack_t *dir_stack, const char *subdir);
int pop(dir_stack_t *dir_stack);
// TODO: 销毁栈
int destory(dir_stack_t *dir_stack);
// TODO: 清空栈
int clean(dir_stack_t *dir_stack);
// TODO: 判断栈空
int empty(dir_stack_t dir_stack);
// TODO: 查看栈顶元素
int peek(dir_stack_t *dir_stack, char *top_dir);
// TODO: 判断目录是否存在
int dir_exist(const dir_stack_t *dir_stack);

// 用户接口
int file_exist(const char *filepath);
/* mkdir rmdir puts gets 拼接上文件名或目录名
  mkdir(pathname)
*/
int get_pathname(char *real_path, const char *arg);
// TODO: 虚拟目录->真实目录，要求调用者为real_dir申请内存
int get_virt_dir(const dir_stack_t *dir_stack, char *virt_dir);
int get_real_dir(const dir_stack_t *dir_stack, char *real_dir);
#endif
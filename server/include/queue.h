/*
 * @Author: Junhui Li junhui.lee@icloud.com
 * @Date: 2023-08-25 22:05:38
 * @LastEditors: Junhui Li junhui.lee@icloud.com
 * @LastEditTime: 2023-08-26 09:20:22
 * @Description:
 */
 #ifndef __QUEUE_H__
 #define __QUEUE_H__
 
typedef struct task_s {
  int netfd;
  char *cmd;
  char *arg;
  struct task_s *next;
} task_t;

typedef struct queue_s {
  task_t *front;
  task_t *rear;
  int length;
} queue_t;


int init_queue(queue_t* p_task_queue);

int enqueue(queue_t* p_task_queue, int netfd, char* cmd, int cmd_size, char* arg, int arg_size);

int dequeue(queue_t* p_task_queue);

#endif
#ifndef __CMD_H__
#define __CMD_H__
#include "directory.h"
#include "queue.h"
typedef enum {
  SERVER_OK,
  SERVER_ERR,
  LOCAL_EXIST,
  LOCAL_NOT_EXIST
} status_t;
// 命令相关的函数声明写这里
int do_cd(dir_stack_t *user_dir, task_t *ptask, status_t *stat);
int do_pwd(const dir_stack_t *user_dir, task_t *ptask, status_t *stat);

int do_ls(char *real_path, task_t *ptask, status_t *stat);
int do_mkdir(char *real_path, task_t *ptask, status_t *stat);
int do_rmdir(char *real_path, task_t *ptask, status_t *stat);
int do_rm(char *real_path, task_t *ptask, status_t *stat);
int do_puts(char *real_path, task_t *ptask, status_t *stat);
int do_gets(char *real_path, task_t *ptask, status_t *stat);

#endif
#ifndef __UTILITY_H__
#define __UTILITY_H__

#define NETDISK_LOG_INFO(logfd, username, msg) {        \
    time_t sec = time(NULL);                            \
    char tm[26];                                        \
    ctime_r(&sec, tm);                                  \
    tm[24] = '\0';                                      \
    char buf[1024] = {0};                               \
    sprintf(buf, "%s, %s %s\n", tm, username, msg);   \
    write(logfd, buf, strlen(buf));                     \
}

int verify(int sockfd, int logfd);

#endif
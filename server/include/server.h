#ifndef __SERVER_H__
#define __SERVER_H__

typedef struct train_s {
    int length;
    char data[1000];
} train_t;


int tcp_init(char*ip, char* port, int* psockfd);

int epoll_add(int epfd, int fd);

int epoll_del(int epfd, int fd);


#endif
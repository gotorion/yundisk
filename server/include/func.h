#ifndef __FUNC_H__
#define __FUNC_H__
#include <arpa/inet.h>
#include <assert.h>
#include <crypt.h>
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <libgen.h>
#include <netdb.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <pthread.h>
#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/epoll.h>
#include <sys/ioctl.h>
#include <sys/ipc.h>
#include <sys/select.h>
#include <sys/shm.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/utsname.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>
#include <shadow.h>
#include <sys/mman.h>
#define SIZE(a) (sizeof(a) / sizeof(a[0]))

#define ARGS_CHECK(argc, n)                                 \
  {                                                         \
    if (argc != n) {                                        \
      fprintf(stderr, "Error: expected %d arguments\n", n); \
      exit(1);                                              \
    }                                                       \
  }

#define ERROR_CHECK(retval, val, msg) \
  {                                   \
    if (retval == val) {              \
      perror(msg);                    \
      exit(1);                        \
    }                                 \
  }

#define THREAD_ERROR_CHECK(ret, msg)                   \
  {                                                    \
    if (ret != 0) {                                    \
      fprintf(stderr, "%s: %s\n", msg, strerror(ret)); \
    }                                                  \
  }

#endif
// TODO: syslog封装
/*
 * @Author: Junhui Li junhui.lee@icloud.com
 * @Date: 2023-08-25 21:57:29
 * @LastEditors: Junhui Li junhui.lee@icloud.com
 * @LastEditTime: 2023-08-26 09:27:01
 * @Description:
 */
#ifndef __THREAD_H__
#define __THREAD_H__
#include "func.h"
#include "queue.h"
#include "directory.h"
#include <sys/syslog.h>
#include <unistd.h>
#include "cmd.h"
#include <stdio.h>

typedef struct threadPool_s {
    pthread_mutex_t mutex;
    pthread_cond_t cond;
    int worker_num;
    queue_t task_queue;
    dir_stack_t user_stack; // TODO: 后续可能改成多用户目录栈的数组
    pthread_t *worker_arr;
    int epfd;
    int logfd;
} threadPool_t;


// (LOG_INFO, "netfd = %d, %s %s, SUCCESS\n", ptask->netfd, ptask->cmd, ptask->arg);  

#define SYSLOG(logfd, ptask, status) {          \
    time_t sec = time(NULL);                    \
    char tm[26];                                \
    ctime_r(&sec, tm);                          \
    tm[24] = '\0';                              \
    char buf[1024] = {0};                       \
    if (status == SERVER_OK) {                  \
        sprintf(buf, "%s, netfd = %d, %s %s, SUCCESS\n", tm, ptask->netfd, ptask->cmd, ptask->arg);                \
        write(logfd, buf, strlen(buf));         \
    } else {                                    \
        sprintf(buf, "%s, netfd = %d, %s %s, FAILURE\n", tm, ptask->netfd, ptask->cmd, ptask->arg);                \
        write(logfd, buf, strlen(buf));         \
    }                                           \
}


int init_thread_pool(threadPool_t* p_thread_pool, int work_num);
int make_worker(threadPool_t* p_thread_pool);
void* threadFunc(void * arg);
int func_select(task_t* task, char* real_path, status_t* status);
int cd_or_pwd(threadPool_t* p_thread_pool, task_t* task, status_t* status);
int task_free(task_t* task);



#endif
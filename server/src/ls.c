#include <bits/types/locale_t.h>
#include <grp.h>
#include <langinfo.h>
#include <pwd.h>
#include <stdio.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <time.h>

#include "../include/cmd.h"
#include "../include/func.h"
#include "../include/queue.h"
#include "../include/server.h"

void send_finfo(int netfd, char *buf);
void change_mode(char *perm, int n, mode_t mode);
int get_finfo(char *buf, const struct stat *info, const char *fname);

int do_ls(char *real_path, task_t *ptask, status_t *status) {
  DIR *pdir = opendir(real_path);
  struct dirent *pdirent;
  while ((pdirent = readdir(pdir))) {
    if (pdirent->d_name[0] == '.') {
      continue;
    }
    char fname[4096];
    sprintf(fname, "%s/%s", real_path, pdirent->d_name);

    struct stat info;
    if (!stat(fname, &info)) {
      char buf[4096] = {0};
      get_finfo(buf, &info, pdirent->d_name);
      send_finfo(ptask->netfd, buf);
    }
  }
  train_t end;
  end.length = 0;
  send(ptask->netfd, &end.length, sizeof(int), 0);

  closedir(pdir);
  return 0;
}

int get_finfo(char *buf, const struct stat *info, const char *fname) {
  // 获取文件类型和用户权限
  char perm[5];
  change_mode(perm, sizeof(perm) / sizeof(perm[0]), info->st_mode);

  // 获取文件最近修改时间
  struct tm *tm = localtime(&info->st_mtim.tv_sec);
  char date[516];
  int mon = tm->tm_mon + 1;
  int day = tm->tm_mday;
  int hour = tm->tm_hour;
  int min = tm->tm_min;
  sprintf(date, "%2d月 %2d %02d:%02d", mon, day, hour, min);

  // 获取硬连接数和文件大小
  nlink_t nlink = info->st_nlink;
  off_t fsize = info->st_size;

  // 合并成一个字符串
  sprintf(buf, "%s %2ld %6ld %s  %-s", perm, nlink, fsize, date, fname);
  // puts(buf);
  return 0;
}

void change_mode(char *perm, int n, mode_t mode) {
  memset(perm, '-', n);
  perm[4] = 0;
  if (S_ISBLK(mode)) perm[0] = 'b';
  if (S_ISCHR(mode)) perm[0] = 'c';
  if (S_ISDIR(mode)) perm[0] = 'd';
  if (S_ISFIFO(mode)) perm[0] = 'p';
  if (S_ISLNK(mode)) perm[0] = 'l';
  if (S_ISREG(mode)) perm[0] = '-';
  if (S_ISSOCK(mode)) perm[0] = 's';
  if (S_IRUSR & mode) perm[1] = 'r';
  if (S_IWUSR & mode) perm[2] = 'w';
  if (S_IXUSR & mode) perm[3] = 'x';
}

void send_finfo(int netfd, char *buf) {
  train_t train;
  bzero(&train, sizeof(train));
  train.length = strlen(buf);
  memcpy(train.data, buf, train.length);
  send(netfd, &train.length, sizeof(int), 0);
  send(netfd, buf, train.length, MSG_NOSIGNAL);
}

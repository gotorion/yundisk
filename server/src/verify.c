#include "../include/utility.h"
#include "../include/func.h"
#include "../include/cmd.h"
static void get_salt(char *salt, char *passwd) {
  int i, j;
  for (i = 0, j = 0; passwd[i] && j != 4; i++) {
    if (passwd[i] == '$') {
      j++;
    }
  }
  strncpy(salt, passwd, i-1);
}

int verify(int sockfd, int logfd) {
    /* 用户接入连接后立即验证，函数返回-1表示验证失败，调用者循环等待重复验证，成功则由函数调用者对fd进行监听
   */
    char user_name[512] = {0};
    char password[512] = {0};
    // 接收用户名和密码
    int length;
    recv(sockfd, &length, sizeof(length), MSG_WAITALL);
    recv(sockfd, user_name, length, MSG_WAITALL);
    recv(sockfd, &length, sizeof(length), MSG_WAITALL);
    recv(sockfd, password, length, MSG_WAITALL);
    // 验证
    char salt[512] = {0};
    struct spwd *sp = getspnam(user_name);
    ERROR_CHECK(sp, NULL, "getspnam");
    get_salt(salt, sp->sp_pwdp);
    // 返回客户端信息
    status_t status;
    if (strcmp(sp->sp_pwdp, crypt(password, salt)) == 0) {
        status = SERVER_OK;
        NETDISK_LOG_INFO(logfd, user_name, "login successful");
    } else {
        status = SERVER_ERR;
        NETDISK_LOG_INFO(logfd, user_name, "login failed");
    }
    send(sockfd, &status, sizeof(status_t), 0);
    return (status == SERVER_OK) ? 0 : -1;
}
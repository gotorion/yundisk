#include "../include/queue.h"
#include "../include/func.h"
#include <string.h>

int init_queue(queue_t* p_task_queue) {
    bzero(p_task_queue, sizeof(queue_t));
    return 0;
}



int enqueue(queue_t* p_task_queue, int netfd, char* cmd, int cmd_size, char* arg, int arg_size) {
    // 创建新的 任务节点
    task_t* p_task = (task_t*) calloc(1, sizeof(task_t));

    p_task->netfd = netfd;

    p_task->cmd = (char*) calloc(cmd_size+1, 1);
    strcpy(p_task->cmd, cmd);

    p_task->arg = (char*) calloc(arg_size+1, 1);
    strcpy(p_task->arg, arg);
    
    // 把 任务节点 添加到 任务队列
    if (p_task_queue->length == 0) {
        p_task_queue->front = p_task;
    } else {
        p_task_queue->rear->next = p_task;
    }
    p_task_queue->rear = p_task;
    ++p_task_queue->length;

    return 0;
}


int dequeue(queue_t* p_task_queue) {
    task_t* p_task = p_task_queue->front;
    p_task_queue->front = p_task->next;
    // free(p_task);

    if (p_task_queue->length == 1) {
        p_task_queue->rear = NULL;
    }

    --p_task_queue->length;

    return 0;
}
#include "../include/cmd.h"
#include "../include/directory.h"
typedef struct train_s {
  int length;
  char data[4096];
} train_t;

/* 传入用户目录栈和cd的参数，如果成功进入目录则压栈并将当前目录返回给用户，否则将错误信息放入err_msg
 */
// TODO:重写cd
int do_cd(dir_stack_t *user_dir, task_t *ptask, status_t *stat) {
  char msg[MSG_LENGTH];
  bzero(msg, MSG_LENGTH);
  if (strncmp(ptask->arg, "..", 2) == 0) {  // 进入上级目录
    pop(user_dir);
    get_virt_dir(user_dir, msg);
  } else if (strncmp(ptask->arg, ".", 1) == 0) {  // 进入当前目录，什么也不做
    get_virt_dir(user_dir, msg);
  } else {  // 进入文件夹
    push(user_dir, ptask->arg);
    if (dir_exist(user_dir)) {
      get_virt_dir(user_dir, msg);
    } else {
      strcpy(msg, "directory does't exit");
      pop(user_dir);  // cd 的参数不是可进入的目录，报错并出栈
      *stat = SERVER_ERR;
    }
  }
  int length = strlen(msg);
  send(ptask->netfd, stat, sizeof(status_t), 0);
  send(ptask->netfd, &length, sizeof(length), 0);
  send(ptask->netfd, msg, length, 0);
  return (*stat == SERVER_OK) ? 0 : -1;
}
/* 返回当前路径 */
int do_pwd(const dir_stack_t *user_dir, task_t *ptask, status_t *stat) {
  char msg[MSG_LENGTH];
  bzero(msg, MSG_LENGTH);
  get_virt_dir(user_dir, msg);
  send(ptask->netfd, stat, sizeof(status_t), 0);
  int length = strlen(msg);
  send(ptask->netfd, &length, sizeof(length), 0);
  send(ptask->netfd, msg, strlen(msg), 0);
  return 0;
}

//
int do_rm(char *real_path, task_t *ptask, status_t *stat) {
  get_pathname(real_path, ptask->arg);
  puts(real_path);
  if (!file_exist(real_path)) {
    *stat = SERVER_ERR;
  } else {
    int ret = unlink(real_path);
    if (ret == -1) {
      *stat = SERVER_ERR;
    }
  }
  send(ptask->netfd, stat, sizeof(status_t), 0);
  return (*stat == SERVER_OK) ? 0 : -1;
}

int do_mkdir(char *real_path, task_t *ptask, status_t *stat) {
  get_pathname(real_path, ptask->arg);
  if (access(real_path, F_OK) == 0) {
    *stat = SERVER_ERR;
  } else {
    int ret = mkdir(real_path, 0777);
    if (ret != 0) {
      *stat = SERVER_ERR;
    }
  }
  send(ptask->netfd, stat, sizeof(status_t), 0);
  return 0;
}

int do_rmdir(char *real_path, task_t *ptask, status_t *stat) {
  get_pathname(real_path, ptask->arg);
  // 递归删除目录
  DIR *pdir = opendir(real_path);
  if (pdir == NULL) {
    *stat = SERVER_ERR;
  } else {
    struct dirent *pdirent = NULL;
    while ((pdirent = readdir(pdir)) != NULL) {
      if (strcmp(pdirent->d_name, "..") != 0 &&
          strcmp(pdirent->d_name, ".") != 0) {
        *stat = SERVER_ERR;
        break;
      }
    }
    int ret = rmdir(real_path);
    if (ret != 0) {
      *stat = SERVER_ERR;
    }
    send(ptask->netfd, stat, sizeof(status_t), 0);
    closedir(pdir);
  }
  return (*stat == SERVER_OK) ? 0 : -1;
}
static int get_remain(int sockfd, int fd) {
  struct stat statbuf;
  fstat(fd, &statbuf);
  off_t local_filesize;
  recv(sockfd, &local_filesize, sizeof(off_t), MSG_WAITALL);
  send(sockfd, &statbuf.st_size, sizeof(off_t), 0);

  char *p = mmap(NULL, statbuf.st_size-local_filesize, PROT_READ, PROT_WRITE, fd, local_filesize);
  send(sockfd, p, statbuf.st_size-local_filesize, 0);
  munmap(p, statbuf.st_size);
  return 0;
}
int do_gets(char *real_path, task_t *ptask, status_t *stat) {
  status_t local_stat;
  recv(ptask->netfd, &local_stat, sizeof(status_t), MSG_WAITALL);
  get_pathname(real_path, ptask->arg);
  if (access(real_path, F_OK) != 0) {
    *stat = SERVER_ERR;
  }
  send(ptask->netfd, stat, sizeof(status_t), 0);
  // 服务端也确认自己文件状态
  if (*stat == SERVER_ERR) {
    return -1;
  } else {
    if (local_stat == LOCAL_EXIST) {
      int fd = open(real_path, O_RDWR);
      get_remain(ptask->netfd, fd);
      return 0;
    } else {
      // 发送文件
      struct stat statbuf;
      int fd = open(real_path, O_RDWR);
      fstat(fd, &statbuf);
      send(ptask->netfd, &statbuf.st_size, sizeof(statbuf.st_size), 0);
      char *p = (char *)mmap(NULL, statbuf.st_size, PROT_READ | PROT_WRITE,
                             MAP_SHARED, fd, 0);
      send(ptask->netfd, p, statbuf.st_size, 0);
      munmap(p, statbuf.st_size);
    }
  }
  return 0;
}

int do_puts(char *real_path, task_t *ptask, status_t *stat) {
  get_pathname(real_path, ptask->arg);
  if (access(real_path, F_OK) == 0) {
    *stat = SERVER_ERR;
    send(ptask->netfd, stat, sizeof(status_t), 0);
    return -1;
  }
  send(ptask->netfd, stat, sizeof(status_t), 0);
  int fd = open(real_path, O_RDWR | O_CREAT, 0666);
  off_t file_size;
  recv(ptask->netfd, &file_size, sizeof(off_t), MSG_WAITALL);
  ftruncate(fd, file_size);
  char *p =
      (char *)mmap(NULL, file_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
  recv(ptask->netfd, p, file_size, MSG_WAITALL);
  munmap(p, file_size);

  return 0;
}
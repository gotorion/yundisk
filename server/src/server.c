#include "../include/thread.h"
#include "../include/server.h"
#include <stdio.h>
#include <strings.h>
#include <sys/socket.h>
#include <threads.h>
#include "../include/utility.h"

int main(int argc, char* argv[]) {

    // exe ip port workernum
    ARGS_CHECK(argc, 4);

    // 线程池的初始化, 与子线程的创建
    threadPool_t thread_pool;
    init_thread_pool(&thread_pool, atoi(argv[3]));
    make_worker(&thread_pool);

    // tcp 初始化
    int sockfd;
    tcp_init(argv[1], argv[2], &sockfd);

    int epfd = epoll_create(1);
    thread_pool.epfd = epfd;
    epoll_add(epfd, sockfd);

    // 打开日志
    int logfd = open("log", O_RDWR | O_CREAT | O_APPEND, 0777);
    thread_pool.logfd = logfd;

    struct epoll_event readyset[1024];
    while (1) {
        int ready_num = epoll_wait(epfd, readyset, SIZE(readyset), -1);

        for (int i=0; i < ready_num; ++i) {
            int fd = readyset[i].data.fd;
            if (fd == sockfd) {
                int netfd = accept(sockfd, NULL, NULL);

                int ret = 1;
                while (ret != 0) {
                  ret = verify(netfd, logfd);
                }
                
                epoll_add(epfd, netfd);

            } else {
                int netfd = fd;
                // 获取命令, 参数
                train_t cmd, arg;
                bzero(&cmd, sizeof(cmd));
                bzero(&arg, sizeof(arg));

                // 接收 命令
                int ret;
                recv(netfd, &cmd.length, sizeof(cmd.length), MSG_WAITALL);
                ret = recv(netfd, cmd.data, cmd.length, MSG_WAITALL);

                // 如果有参数, 接收参数
                if (strcmp(cmd.data, "ls") && strcmp(cmd.data, "pwd")) {
                    recv(netfd, &arg.length, sizeof(arg.length), MSG_WAITALL);
                    ret = recv(netfd, arg.data, arg.length, MSG_WAITALL);
                }

                // 客户端断开连接, 关闭 netfd, 并从 epoll 队列移除 
                if (ret == 0) {
                    char fd_arr[1024] = {0};
                    sprintf(fd_arr, "netfd = %d", netfd);
                    NETDISK_LOG_INFO(logfd, fd_arr, "quit");

                    clean(&thread_pool.user_stack);
                    epoll_del(epfd, netfd);
                    close(netfd);
                    continue;
                }

                // 加锁, cmd arg 入队
                pthread_mutex_lock(&thread_pool.mutex);
                enqueue(&thread_pool.task_queue, netfd, cmd.data, cmd.length,
                        arg.data, arg.length);

                // 从 epoll 监听中 移除 netfd
                epoll_del(epfd, netfd);


                // 打印 cmd arg
                // if (strcmp(cmd.data, "ls") && strcmp(cmd.data, "pwd")) {
                //     printf("%s %s\n", thread_pool.task_queue.rear->cmd,
                //            thread_pool.task_queue.rear->arg);
                // } else {
                //     printf("%s\n", thread_pool.task_queue.rear->cmd);
                // }

                pthread_cond_signal(&thread_pool.cond);
                pthread_mutex_unlock(&thread_pool.mutex);
            }
        }
    }

    return 0;
}


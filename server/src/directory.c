#include "../include/directory.h"
/* 初始化用户目录栈 */
int init_dir_stack(dir_stack_t *dir_stack) {
  memset(dir_stack, 0, sizeof(dir_stack_t));
  dir_stack->top = -1;
  return 0;
}
/* 将下一级目录压栈，保存的目录名入栈时分配，出栈时free */
int push(dir_stack_t *dir_stack, const char *subdir) {
  if (dir_stack->top >= DEPTH - 1) {
    return -1;
  }
  char *new_dir = calloc(50, sizeof(char));
  strcpy(new_dir, subdir);
  ++dir_stack->top;
  dir_stack->dirs[dir_stack->top] = new_dir;
  return 0;
}
/* 弹栈 */
int pop(dir_stack_t *dir_stack) {
  if (dir_stack->top == -1) {
    return -1;
  }
  free(dir_stack->dirs[dir_stack->top]);
  dir_stack->top--;
  return 0;
}
/* 销毁栈 */
// TODO:
int destory(dir_stack_t *dir_stack) {
  return 0;
}
/* 清空栈，多用户下复用 */
int clean(dir_stack_t *dir_stack) {
  if (empty(*dir_stack)) {
    return 0;
  } else {
    while (dir_stack->top >= 0) {
      free(dir_stack->dirs[dir_stack->top]);
      dir_stack->top--;
    }
    return 0;
  }
}
/* 判断栈空 */
int empty(dir_stack_t dir_stack) {
  if (dir_stack.top == -1) {
    return 1;
  } else {
    return 0;
  }
}
/* 查看栈顶元素 */
int peek(dir_stack_t *dir_stack, char *top_dir) {
  if (dir_stack->top == -1) {
    return -1;
  }
  printf("top = %s\n", dir_stack->dirs[dir_stack->top]);
  return 0;
}
int dir_exist(const dir_stack_t *dir_stack) {
  char real_path[512] = {0};
  get_real_dir(dir_stack, real_path);
  int ret = access(real_path, F_OK);
  if (ret != 0) {
    return 0;
  }
  struct stat statbuf;
  stat(real_path, &statbuf);
  if (S_ISDIR(statbuf.st_mode)) {
    return 1;
  }
  return 0;
}
int file_exist(const char *filepath) {
  int ret = access(filepath, F_OK);
  if (ret != 0) {
    return 0;
  }
  struct stat statbuf;
  stat(filepath, &statbuf);
  if (S_ISREG(statbuf.st_mode)) {
    return 1;
  }
  return 0;
}
int get_pathname(char *real_path, const char *arg) {
  strcat(real_path, "/");
  strcat(real_path, arg);
  return 0;
}
/* 获取虚拟地址pwd */
int get_virt_dir(const dir_stack_t *dir_stack, char *virt_dir) {
  memset(virt_dir, 0, MSG_LENGTH);
  strcat(virt_dir, "~");
  for (int i = 0; i <= dir_stack->top; i++) {
    strcat(virt_dir, "/");
    strcat(virt_dir, dir_stack->dirs[i]);
  }
  return 0;
}
/* 从栈里的虚拟地址获取真实地址 */
int get_real_dir(const dir_stack_t *dir_stack, char *real_dir) {
  memset(real_dir, 0, MSG_LENGTH);
  char * pwd = getcwd(NULL, 0); // 需要free
  strcat(real_dir, pwd);
  for (int i = 0; i <= dir_stack->top; i++) {
    strcat(real_dir, "/");
    strcat(real_dir, dir_stack->dirs[i]);
  }
  free(pwd);
  return 0;
}
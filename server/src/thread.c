#include "../include/thread.h"

#include "../include/cmd.h"
#include "../include/server.h"

int init_thread_pool(threadPool_t* p_thread_pool, int worker_num) {
    pthread_mutex_init(&p_thread_pool->mutex, NULL);
    pthread_cond_init(&p_thread_pool->cond, NULL);
    p_thread_pool->worker_num = worker_num;
    init_queue(&p_thread_pool->task_queue);
    // 初始化用户栈
    init_dir_stack(&p_thread_pool->user_stack);
    p_thread_pool->worker_arr =
        (pthread_t*)calloc(worker_num, sizeof(pthread_t));
    return 0;
}

int make_worker(threadPool_t* p_thread_pool) {
    for (int i = 0; i < p_thread_pool->worker_num; ++i) {
        pthread_create(&p_thread_pool->worker_arr[i], NULL, threadFunc,
                       p_thread_pool);
    }

    return 0;
}

void* threadFunc(void* arg) {
    threadPool_t* p_thread_pool = (threadPool_t*)arg;

    while (1) {
        pthread_mutex_lock(&p_thread_pool->mutex);

        while (p_thread_pool->task_queue.length == 0) {
            pthread_cond_wait(&p_thread_pool->cond, &p_thread_pool->mutex);
        }

        task_t* task = p_thread_pool->task_queue.front;
        dequeue(&p_thread_pool->task_queue);


        // 传入用户栈, 获取真实地址
        char* real_path = (char*)calloc(MSG_LENGTH, 1);
        get_real_dir(&p_thread_pool->user_stack, real_path);

        // 用于获取功能参数运行状态
        status_t status = SERVER_OK;

        // cd pwd 命令访问 用户目录栈, 单独处理
        int ret = cd_or_pwd(p_thread_pool, task, &status);
        // 返回值为 0, 表示该条命令已处理(为 cd 或 pwd)
        if (ret == 0) {
            continue;
        }

        pthread_mutex_unlock(&p_thread_pool->mutex);

        // 根据 ls pwd mkdir rmdir puts gets rm 选择对应的 功能函数
        func_select(task, real_path, &status);
        SYSLOG(p_thread_pool->logfd, task, status);

        // 子线程完成任务, 把 netfd 重新加入 epoll 监听
        epoll_add(p_thread_pool->epfd, task->netfd);

        task_free(task);
    }
}

int cd_or_pwd(threadPool_t* p_thread_pool, task_t* task, status_t* status) {
    if (!strcmp(task->cmd, "cd")) {
        do_cd(&p_thread_pool->user_stack, task, status);
    } else if (!strcmp(task->cmd, "pwd")) {
        do_pwd(&p_thread_pool->user_stack, task, status);
    } else {
        // 返回 -1 表示 cmd 不是 cd pwd
        return -1;
    }

    pthread_mutex_unlock(&p_thread_pool->mutex);
    epoll_add(p_thread_pool->epfd, task->netfd);
    SYSLOG(p_thread_pool->logfd, task, *status);
    task_free(task);

    return 0;
}

// 根据 ls pwd mkdir rmdir puts gets rm 选择对应的 功能函数
int func_select(task_t* task, char* real_path, status_t* status) {
    if (!strcmp(task->cmd, "ls")) {
        do_ls(real_path, task, status);
    } else if (!strcmp(task->cmd, "mkdir")) {
        do_mkdir(real_path, task, status);
    } else if (!strcmp(task->cmd, "rmdir")) {
        do_rmdir(real_path, task, status);
    } else if (!strcmp(task->cmd, "puts")) {
        do_puts(real_path, task, status);
    } else if (!strcmp(task->cmd, "gets")) {
        do_gets(real_path, task, status);
    } else if (!strcmp(task->cmd, "rm")) {
        do_rm(real_path, task, status);
    }

    return 0;
}

int task_free(task_t* task) {
    free(task->cmd);
    free(task->arg);
    free(task);

    return 0;
}
